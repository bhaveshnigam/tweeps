"""Project wide constants stored here."""

__author__ = 'bhaveshnigam'



# Key names associated to twitter authentication.
TWITTER_CONSUMER_KEY = 'twitter_consumer_key'
TWITTER_CONSUMER_SECRET = 'twitter_consumer_secret'
TWITTER_ACCESS_TOKEN = 'twitter_access_token'
TWITTER_ACCESS_TOKEN_SECRET = 'twitter_access_token_secret'
