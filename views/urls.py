import webapp2

import views

def GetRoutes():
  return [
      ('/', views.Index),
      ('/tweets', views.GetTweets),
  ]
