from google.appengine.ext import ndb

__author__ = 'bhaveshnigam'


class AuthToken(ndb.Model):
  """Model to store values for required auth tokens for different APIs."""
  token_name = ndb.StringProperty(required=True)
  token_value = ndb.StringProperty()
