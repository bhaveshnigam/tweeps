import json
import logging

from third_party import twitter

from models import AuthToken
import constants
from helpers import BaseHandler


__author__ = 'bhaveshnigam'


class Index(BaseHandler):

  def get(self):
    self.render_template('home.html')


class GetTweets(BaseHandler):

  def get(self):
    self.response.headers['Content-Type'] = 'application/json'

    self.twitter_api = self._GetTwitterApiInstance()
    if not isinstance(self.twitter_api, twitter.api.Api):
      return self.twitter_api

    latitude = self.request.get('latitude')
    longitude = self.request.get('longitude')
    radius = self.request.get('radius')

    tweets = self.GetLocalTweets(
      latitude=latitude, longitude=longitude, radius=radius)

    self.response.headers['Content-Type'] = 'application/json'
    return self.response.write(json.dumps(tweets))

  def GetLocalTweets(self, latitude, longitude, radius):
    tweets = {}
    twitter_response = self.twitter_api.GetSearch(
        geocode=(latitude, longitude, radius),
        count=20,
        result_type='recent')
    for tweet in twitter_response:
      tweets[tweet.GetId()] = {'user': tweet.GetUser().AsDict(),
                               'content': tweet.GetText(),
                               'coordinates': tweet.GetCoordinates()}
    return tweets

  def _GetTwitterApiInstance(self):
    """Get twitter API instance to interface with """

    twitter_consumer_key = AuthToken.query(
      AuthToken.token_name==constants.TWITTER_CONSUMER_KEY).get()
    twitter_consumer_secret = AuthToken.query(
      AuthToken.token_name==constants.TWITTER_CONSUMER_SECRET).get()
    twitter_access_token = AuthToken.query(
      AuthToken.token_name==constants.TWITTER_ACCESS_TOKEN).get()
    twitter_access_token_secret = AuthToken.query(
      AuthToken.token_name==constants.TWITTER_ACCESS_TOKEN_SECRET).get()

    if None in [twitter_consumer_key, twitter_consumer_secret,
                twitter_access_token, twitter_access_token_secret]:

      # Create a dummy entry in datastore to populate namespace, so that the
      # keys can be entered manually through the admin interface.
      dummy_key = AuthToken.query(AuthToken.token_name=='dummy').get()
      if not dummy_key:
        AuthToken(token_name='dummy', token_value='dummy').put()

      self.response.write(
        'Please provide valid authentication keys in datastore.')
      return self.response.set_status(
        404, message='Please provide valid authentication keys in datastore.')


    api = twitter.Api(
        consumer_key=twitter_consumer_key.token_value,
        consumer_secret=twitter_consumer_secret.token_value,
        access_token_key=twitter_access_token.token_value,
        access_token_secret=twitter_access_token_secret.token_value)

    try:
      twitter_user = api.VerifyCredentials()
      logging.info(
        'Created Twitter API instance with User: %s' %
        (twitter_user.AsDict().get('screen_name')))
      return api
    except twitter.TwitterError as error:
      error_msg = error[0].get('message')
      self.response.write('Twitter Error: ', error_msg)
      return self.response.set_status(401)
