Tweeps = {};


/**
 * Global variable to store the google map instance
 * @type {google.maps.Map}
 */
Tweeps.MAP;

/**
 * Global variable to store the LatLong position fetch from the browser for the
 * current user.
 */
Tweeps.CURRENT_LOCATION;


/**
 * Configuration level settings for twitter API.
 * @type {!Object}
 */
Tweeps.TWEETS = {
    REFRESH_INTERVAL: 20000,
    MONTITORING_RADIUS: '1mi',
    MONITORING_RADIUS_IN_METERS: 1609.3
};


Tweeps.onPageLoad = function() {
  navigator.geolocation.getCurrentPosition(Tweeps.drawMap);
  Tweeps.attachEventListeners();
};


/**
 * Draws a Google map with the current location marker and 1mile radius.
 * @param position The browser fetched current location of the user.
 */
Tweeps.drawMap = function(position) {
  position.coords.latitude = position.coords.latitude;
  position.coords.longitude = position.coords.longitude;
  Tweeps.CURRENT_LOCATION = position;
  var currentLatLng = new google.maps.LatLng(
        position.coords.latitude,
        position.coords.longitude);

    var mapOptions = {
      center: currentLatLng,
      zoom: 15
    };
    Tweeps.MAP = new google.maps.Map(
        document.getElementById('google-maps'), mapOptions);

    var currentLocationMarker = new google.maps.Marker({
        position: currentLatLng,
        map: Tweeps.MAP,
        title: 'Current Location',
        animation: google.maps.Animation.DROP,
        icon: 'http://wcdn3.dataknet.com/static/resources/icons/set28/58aac1c.png'
    });

    var currentLocationInfoWindow = new google.maps.InfoWindow({
      content: '<div>This is your current location.</div>'
    });

    google.maps.event.addListener(currentLocationMarker, 'click', function() {
      currentLocationInfoWindow.open(Tweeps.MAP, currentLocationMarker);
    });

    // Add circle overlay and bind to marker
    var circle = new google.maps.Circle({
      map: Tweeps.MAP,
      radius: Tweeps.TWEETS.MONITORING_RADIUS_IN_METERS,
      strokeColor: '#000088',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#000088',
      fillOpacity: 0.35
    });
    circle.bindTo('center', currentLocationMarker, 'position');
    twitterApi = new Tweeps.Twitter(Tweeps.MAP);
    twitterApi.initFetchTweets();
};


/**
 * Bind all event listeners.
 */
Tweeps.attachEventListeners = function() {
  $(document.body).ready(function (event) {
    twitterApi = new Tweeps.Twitter(Tweeps.MAP);
    setInterval(
        twitterApi.initFetchTweets.bind(twitterApi),
        Tweeps.TWEETS.REFRESH_INTERVAL);
  });
};


/**
 * Twitter API interface object.
 * @constructor
 */
Tweeps.Twitter = function(gMap){
  /**
   * The Google map instance on which tweet markers will be placed.
   * @type {google.maps.Map}
   */
  this.map = gMap;

  /**
   * Object to store the tweets fetched and placed on the map.
   * @type {Object}
   */
  this.tweets = {};

  /**
   * Object to store the markers currently stored on the map.
   * @type {Array<google.maps.Marker>}
   */
  this.tweetMarkers = [];
};


/**
 * An Auto Polling service to fetch tweets from the twitter API.
 * Configuration fetched from Tweeps.TWEETS.
 */
Tweeps.Twitter.prototype.initFetchTweets = function() {
  var self = this;
  var formData = {
      'latitude': Tweeps.CURRENT_LOCATION.coords.latitude,
      'longitude': Tweeps.CURRENT_LOCATION.coords.longitude,
      'radius': Tweeps.TWEETS.MONTITORING_RADIUS
  }
  var getRequest = $.get('/tweets', formData);
  getRequest.success (function (data) {
    self.createMarkers_(data);
  });
};

/**
 * Creates markers on Google map for a particular Tweet.
 * @param tweets The tweets fetched from the twitter API.
 * @private
 */
Tweeps.Twitter.prototype.createMarkers_ = function(tweets) {
  for (var tweetId in tweets) {
    var tweet = tweets[tweetId];
    if (tweetId in this.tweets) {
      continue;
    }
    // Prevent adding markers more than 100 on the map.
    if (this.tweetMarkers.length > 100) {
      var oldTweetMarker = this.tweetMarkers.shift();
      // Remove the marker from the map.
      oldTweetMarker.setMap(null);
    }
    this.tweets[tweetId] = tweet;
    var latLong = new google.maps.LatLng(
        tweet.coordinates.coordinates[1],
        tweet.coordinates.coordinates[0]
    );

    var marker =  new google.maps.Marker({
        position: latLong,
        map: Tweeps.MAP,
        title: 'Tweet By ' + tweet.user.screen_name,
        animation: google.maps.Animation.DROP
    });
    this.createInfoWindowForTweet_(tweet, marker);
    this.tweetMarkers.push(marker);
  }
};


/**
 * Create an info window to show the contents of a tweet, and bind it
 * to the marker.
 * @param {Object} tweet The tweet object for which the info window needs to be
 *     made. This object contains the information regarding a particular tweet
 *     as provided by the Server Twitter API,
 * @param {google.maps.Marker} marker The marker object to which the info
 *     window will bound to.
 * @private
 */
Tweeps.Twitter.prototype.createInfoWindowForTweet_ = function(tweet, marker) {
  var infoWindowContent = $('#infoWindow').clone();
  infoWindowContent.find('.tweetTitle').html("Tweet By: " + tweet.user.screen_name)
  infoWindowContent.find('#tweetContent').html("Tweeted: " + tweet.content)

  var infoWindow = new google.maps.InfoWindow({
      content: infoWindowContent.html()
  });

  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.open(this.map, marker);
  });
};
