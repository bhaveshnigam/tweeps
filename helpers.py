import os
import jinja2
import webapp2

import logging

__author__ = 'bhaveshnigam'


TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATE_DIR),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class BaseHandler(webapp2.RequestHandler):

  def render_template(self, template_name, template_values=None):
    """Renders Jinja2 template."""
    if template_values == None:
      template_values = {}
    template = JINJA_ENVIRONMENT.get_template(template_name)
    self.response.write(template.render(template_values))

  def handle_exception(self, exception, debug):
    # Log the error.
    logging.exception(exception)

    # Set a custom message.
    self.response.write('An error occurred.')

    # If the exception is a HTTPException, use its error code.
    # Otherwise use a generic 500 error code.
    if isinstance(exception, webapp2.HTTPException):
      self.response.set_status(exception.code)
    else:
      self.response.set_status(500)
