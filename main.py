import site
import os.path
site.addsitedir(os.path.join(os.path.dirname(__file__), 'third_party'))

import webapp2
from views import urls

__author__ = 'bhaveshnigam'


app = webapp2.WSGIApplication(routes=urls.GetRoutes(), debug=True)
